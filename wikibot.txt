%%MODULE%%

%MODULE NAME%
Energy hub model for design, sizing and operation of an energy system of a building or an aggregation of buildings

%FILE NAMES%
Energy_Hub.ams

%DESCRIPTION%
In this repository an AIMMS model for an energy hub model is presented. The model can be used for the optimal design, sizing and operation of an energy system. This energy system could be a single building or an aggregation/cluster of buildings. The loads that are currently covered are heating and cooling loads.

%DOCUMENTATION URL%
https://bitbucket.org/empa_lbst/energy-hub-model/overview/

%RELATED PUBLICATIONS%
Mavromatidis, G., Evins, R., Orehounig, K., Dorer, V., Carmeliet, J. (2014) Multi-objective optimization to simultaneously address energy hub layout, sizing and scheduling using a linear formulation. In: 4th International Conference on Engineering Optimization (ENGOPT), September 8-11, 2014, Lisbon, Portugal,  603-608, CRC Press.

%LICENSE TYPE%

%AUTHORS%
Georgios Mavromatidis

%TAGS%
energy hub model, aimms, milp, optimisation

%DERIVATION OF%
